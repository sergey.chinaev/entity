package generator_test

import (
	"bytes"
	"fmt"
	"gitlab.com/golight/entity/generator"
	"go/format"
	"io"
	"log"
	"os"
	"testing"
)

func TestWhenThereAreNoMethods(t *testing.T) {
	createFiles("./test_files/withoutmethods")

	generator.GenerateMethodsToFile("./test_files/withoutmethods.go")

	if !compareFiles("./test_files/withoutmethods.go", "./test_files/reference.go") {
		t.Errorf("Wrong")
	}

	err := os.Remove("./test_files/withoutmethods.go")
	if err != nil {
		t.Errorf("Remove err: %v", err)
	}
}

func TestWhenThereAreMethods(t *testing.T) {
	createFiles("./test_files/withmethods")

	generator.GenerateMethodsToFile("./test_files/withmethods.go")

	if !compareFiles("./test_files/withmethods.go", "./test_files/reference.go") {
		t.Errorf("Wrong")
	}

	err := os.Remove("./test_files/withmethods.go")
	if err != nil {
		t.Errorf("Remove err: %v", err)
	}
}

func createFiles(path string) {
	f, err := os.Open(path)
	if err != nil {
		log.Fatalf("Failed to open file: %v", err)
	}

	content, err := io.ReadAll(f)
	if err != nil {
		log.Fatalf("Failed to read file: %v", err)
	}

	newFile, err := os.Create(fmt.Sprintf("%v.go", path))
	if err != nil {
		log.Fatalf("Failed to create file: %v", err)
	}
	defer newFile.Close()

	_, err = newFile.Write(content)
	if err != nil {
		log.Fatalf("Failed to write to file: %v", err)
	}
}

func compareFiles(file1, file2 string) bool {
	f1, err := os.Open(file1)
	if err != nil {
		log.Fatalf("Failed to open file: %v", err)
	}

	content1, err := io.ReadAll(f1)
	if err != nil {
		log.Fatalf("Failed reading file %s: %v", file1, err)
	}

	f2, err := os.Open(file2)
	if err != nil {
		log.Fatalf("Failed to open file: %v", err)
	}

	content2, err := io.ReadAll(f2)
	if err != nil {
		log.Fatalf("Failed reading file %s: %v", file1, err)
	}

	content1, err = format.Source(content1)
	if err != nil {
		log.Fatalf("%v", err)
	}

	content2, err = format.Source(content2)
	if err != nil {
		log.Fatalf("%v", err)
	}

	return bytes.Equal(content1, content2)
}
