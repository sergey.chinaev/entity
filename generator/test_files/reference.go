package test_files

import "time"

type Reference struct {
	testFiled1 string `db:"test_filed_1"`
	testFiled2 int    `db:"test_filed_2"`
	testFiled3 bool   `db:"test_filed_3"`
	testFiled4 time.Time
	testFiled5 string `db:""`
	testFiled6 int    `db:"test_filed_6"`
}

func (r *Reference) TableName() string {
	return "references"
}

func (r *Reference) OnCreate() []string {
	return []string{}
}

func (r *Reference) FieldsPointers() []interface{} {
	return []interface{}{
		&r.testFiled1,
		&r.testFiled2,
		&r.testFiled3,
		&r.testFiled4,
		&r.testFiled5,
		&r.testFiled6,
	}
}
