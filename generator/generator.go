package generator

import (
	"fmt"
	"go/ast"
	"go/parser"
	"go/token"
	"log"
	"os"
	"strings"
)

// FieldInfo contains information about a field in a struct.
// Each field has a Name representing the field's name and a Type representing the field's type.
type FieldInfo struct {
	Name string
	Type string
}

// MethodInfo holds details about a method within a struct.
// It includes the Receiver's name and the Name of the method.
type MethodInfo struct {
	Receiver string
	Name     string
}

// ReflectedStruct encapsulates metadata about a struct.
// It includes the StructName, whether HasDBTag is set, and slices for Fields and Methods.
type ReflectedStruct struct {
	StructName string
	HasDBTag   bool
	Fields     []FieldInfo
	Methods    []MethodInfo
}

// ReflectFile inspects the AST of a Go file and returns a slice of ReflectedStruct,
// which contain information about each struct found in the file along with its fields and methods.
// An error is returned if parsing fails.
func ReflectFile(fileName string) ([]ReflectedStruct, error) {
	fset := token.NewFileSet()
	node, err := parser.ParseFile(fset, fileName, nil, parser.ParseComments)
	if err != nil {
		return nil, err
	}

	var reflectDataList []ReflectedStruct

	ast.Inspect(node, func(n ast.Node) bool {
		switch t := n.(type) {
		case *ast.TypeSpec:
			var rData ReflectedStruct
			rData.StructName = t.Name.Name

			s, ok := t.Type.(*ast.StructType)
			if ok {
				for _, field := range s.Fields.List {
					if len(field.Names) == 0 {
						continue
					}
					fieldName := field.Names[0].Name
					fieldType := fmt.Sprintf("%s", field.Type)
					if field.Tag != nil {
						tagValue := field.Tag.Value
						if strings.Contains(tagValue, "db:\"") {
							rData.HasDBTag = true
						}
					}
					rData.Fields = append(rData.Fields, FieldInfo{
						Name: fieldName,
						Type: fieldType,
					})
				}
			}

			reflectDataList = append(reflectDataList, rData)

		case *ast.FuncDecl:
			if t.Recv != nil && len(t.Recv.List) == 1 {
				starExpr, ok := t.Recv.List[0].Type.(*ast.StarExpr)
				if ok {
					ident, ok := starExpr.X.(*ast.Ident)
					if ok {
						// Update the corresponding ReflectedStruct for this receiver
						for idx, rData := range reflectDataList {
							if rData.StructName == ident.Name {
								reflectDataList[idx].Methods = append(reflectDataList[idx].Methods, MethodInfo{
									Receiver: ident.Name,
									Name:     t.Name.Name,
								})
							}
						}
					}
				}
			}
		}
		return true
	})

	return reflectDataList, nil
}

// GenerateMethods generates method definitions for structs with a DB tag in the given file.
// It returns a string containing the generated methods.
func GenerateMethods(filepath string) string {
	// Extract information about structs and methods
	var err error
	var reflectedStructs []ReflectedStruct
	reflectedStructs, err = ReflectFile(filepath)
	if err != nil {
		panic(err)
	}

	var res strings.Builder
	// Generate methods on reflectedStructs data extracted from file
	for _, st := range reflectedStructs {
		if !st.HasDBTag {
			continue
		}
		res.WriteString(generateTablerMethods(st))
	}

	return res.String()
}

// GenerateMethodsToFile appends the generated methods to the specified file path.
// Any error during the file operation is logged.
func GenerateMethodsToFile(filepath string) {
	err := appendToFile(filepath, GenerateMethods(filepath))
	if err != nil {
		log.Printf("Error while generating methods: %v", err)
	}
}

// generateTablerMethods creates a string representation of methods for the given ReflectedStruct.
// It checks for the existence of certain methods and generates them if they don't exist.
func generateTablerMethods(data ReflectedStruct) string {
	receiver := strings.ToLower(data.StructName[:1])
	builder := &strings.Builder{}

	if !methodExists(data.Methods, data.StructName, "TableName") {
		// Generate TableName method
		fmt.Fprintf(builder, "func (%s *%s) TableName() string {\n", receiver, data.StructName)
		fmt.Fprintf(builder, "\treturn \"%ss\"\n", strings.ToLower(data.StructName))
		builder.WriteString("}\n\n")
	}

	if !methodExists(data.Methods, data.StructName, "OnCreate") {
		// Generate OnCreate method
		fmt.Fprintf(builder, "func (%s *%s) OnCreate() []string {\n", receiver, data.StructName)
		builder.WriteString("\treturn []string{}\n")
		builder.WriteString("}\n\n")
	}

	if !methodExists(data.Methods, data.StructName, "FieldsPointers") {
		// Generate FieldsPointers method
		fmt.Fprintf(builder, "func (%s *%s) FieldsPointers() []interface{} {\n", receiver, data.StructName)
		builder.WriteString("\treturn []interface{}{\n")
		for _, field := range data.Fields {
			fmt.Fprintf(builder, "\t\t&%s.%s,\n", receiver, field.Name)
		}
		builder.WriteString("\t}\n")
		builder.WriteString("}\n\n")
	}

	return builder.String()
}

// methodExists checks if a method exists in the list of methods for a given struct.
// Returns true if the method exists, false otherwise.
func methodExists(methods []MethodInfo, structName, methodName string) bool {
	for _, m := range methods {
		if m.Receiver == structName && m.Name == methodName {
			return true
		}
	}

	return false
}

// appendToFile appends the given content string to the specified filename.
// It returns an error if the file operation fails.
func appendToFile(filename, content string) error {
	f, err := os.OpenFile(filename, os.O_APPEND|os.O_WRONLY, 0644)
	if err != nil {
		return err
	}
	defer f.Close()

	if _, err := f.WriteString(content); err != nil {
		return err
	}
	return nil
}
