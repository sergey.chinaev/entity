package tabler

type Tabler interface {
	TableName() string
	OnCreate() []string
	FieldsPointers() []interface{}
}
